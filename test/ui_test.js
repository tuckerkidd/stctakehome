//Load dependencies
var expect = require('chai').expect,
test = require('selenium-webdriver/testing'),
webdriver = require('selenium-webdriver'),
path = require('chromedriver').path,
driver1 = undefined,
driver2 = undefined;
driver3 = undefined
    
describe('STC Test Suite', function(){
    this.timeout(10000);
    before(function(){
        driver1 = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();
        driver2 = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();
        driver3 = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();
        //Get dropdown
        driver1.get('http://the-internet.herokuapp.com/dropdown');
        var dropdown = driver1.findElement(webdriver.By.id('dropdown'));
        
        //Get Start button
        driver2.get('http://the-internet.herokuapp.com/dynamic_loading/1');
        driver3.get('https://admin:admin@the-internet.herokuapp.com/basic_auth');
        return dropdown.findElement(webdriver.By.css("option[value='2']")).click().then(function(){
            return driver2.findElement(webdriver.By.xpath('.//*[@id="start"]/button')).click()
                
            
        }); 
        
    });
     after(function(){
        return driver1.quit().then(function(){
            return driver2.quit().then(function(){
                return driver3.quit();
            });
        });
     });
    it('should select "Option 2" from the dropdown', function() {
         return driver1.findElement(webdriver.By.id('dropdown')).getAttribute('value').then(function(value){ 
            expect(value).to.equal('2'); 
        });        
    });


    it ('should handle dynamic loading with hidden element', function(){
       return driver2.wait(function(){
            return driver2.findElement(webdriver.By.id('finish')).findElement(webdriver.By.tagName('h4')).getAttribute('innerHTML')
            .then(function(value){
                expect(value).to.equal('Hello World!');
                return true;
            });
        },8000);
    });

    it ('should pass a basic auth', function(){
        return driver3.findElement(webdriver.By.id('content')).findElement(webdriver.By.tagName('p')).getAttribute('innerHTML').then(function(value){
            expect(value).to.equal('\n    Congratulations! You must have the proper credentials.\n  ');
        });
    });
});