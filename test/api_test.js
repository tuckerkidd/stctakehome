var expect = require('chai').expect,
request = require('supertest'),
api = require('../api/REST_service');
util = require('../utils/util')


describe ('regres-in', function() {
    it ('get', function(done) {
        api.get()
        .expect(200)
        .end(function(err, response){
            if (err) return done(err);
            expect(response.body).to.deep.equal({
                page:2,
                per_page:3,
                total:12,
                total_pages:4,
                data: [{
                    id:4,
                    first_name:"Eve",
                    last_name:"Holt",
                    avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg" 
                }, {
                    id:5,
                    first_name:"Charles",
                    last_name:"Morris",
                    avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/stephenmoon/128.jpg"
                }, {
                    id:6,
                    first_name:"Tracey",
                    last_name:"Ramos",
                    avatar:"https://s3.amazonaws.com/uifaces/faces/twitter/bigmancho/128.jpg"
                }]
            });
            
            done();
        });
       
       
        
    });

     it('returns a 201 when creating a user', function addUser(done) {
         var payload = util.getRequestObject('morpheus', 'leader')
         api.post(payload)
         .expect(201)
         .end(function(err, response) {
            if (err) return done(err);
            expect(response.body.name).to.equal('morpheus');
            expect(response.body.job).to.equal('leader');
            expect(response.body.createdAt).to.exist;
            done();
         });
     });

     it('returns a 200 when updating a user', function updateUser(done) {
         var payload = util.getRequestObject('morpheus', 'zion resident')

         api.put(payload)
         .expect(200)
         .end(function(err, response) {
            if (err) return done(err);
            expect(response.body.job).to.equal('zion resident');
            expect(response.body.updatedAt).to.exist;
            done();
         });

        
        });
        it ('returns a 204 when deleting a user', function deleteUser(done) {
            var userID = 2
            api.delete(userID)
            .expect(204)
            .end(function(err, response) {
                if (err) return done(err);
                expect(response.body.job).to.be.undefined;
                done();

    });
});
});
