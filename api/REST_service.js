var request = require('supertest');
var serviceHost = 'https://reqres.in';
var path = '/api/users'

exports.get = function() {
    return request(serviceHost)
    .get(path + '?page=2')
};

exports.post = function(payload) {
    return request(serviceHost)
    .post(path)
    .send(payload)
};

exports.put= function(payload) {
    return request(serviceHost)
    .put(path + '/2')
    .send(payload)
};

exports.delete= function(userID){
    return request(serviceHost)
    .delete(path + '/' + userID)
};


